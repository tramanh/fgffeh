package com.forgetfilefast;
public class Settings {

    private static volatile Settings instance;
    private static Object mutex = new Object();
    public static String STORAGE;
    public static int PORT;

    private Settings() {
    }
    
    public static Settings getInstance() {
        Settings result = instance;
        if (result == null) {
            synchronized (mutex) {
                result = instance;
                if (result == null)
                    instance = result = new Settings();
            }
        }
        return result;
    }
}