package com.forgetfilefast;
import com.sun.net.httpserver.*;

import java.io.*;
import java.net.*;
import java.util.*;

public class AppSrv {
    private int PORT;
    private static String STORAGE;

    public static void main(String[] args) throws Exception {
        System.out.println("Hello Server!");
        System.out.println("Usage: java AppSrv <port> <storage's absolute path>");

        if (args.length < 2)
            return;
        int PORT = Integer.parseInt(args[0]);
        String STORAGE = args[1];

        Settings settings = Settings.getInstance();
        settings.PORT = PORT;
        settings.STORAGE = STORAGE;

        HttpServer httpserver = new AppSrv().run();
        httpserver.stop(0);
    }


    public HttpServer run() throws Exception {
//        HttpServer  server = HttpServer.create(new InetSocketAddress(InetAddress.getLoopbackAddress(), 8000), 1000); // 0- default
        HttpServer  server = HttpServer.create(new InetSocketAddress(Settings.getInstance().PORT), 1000); // 0- default

        HttpContext ctxDemo = server.createContext("/demo", new MyHttpHandler());
        ctxDemo.getFilters().add(new ParameterFilter());
        HttpContext ctxTest = server.createContext("/test", new MyTestHttpHandler());
        ctxTest.getFilters().add(new ParameterFilter());
        HttpContext ctxDl = server.createContext("/dl", new DownloadHandler());
        ctxDl.getFilters().add(new ParameterFilter());
        server.setExecutor(java.util.concurrent.Executors.newCachedThreadPool());
        server.start();
        return server;
    }
    class MyHttpHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            String response = "Hello, Word\n";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    class MyTestHttpHandler implements HttpHandler {
        /*
         - no database
         - file name must be valid linux name
         */
        public void handle(HttpExchange ex) throws IOException {
            String res = "Test\n";
            URI requestURI = ex.getRequestURI();
//            ex.getResponseHeaders().set("Content-Type", "text/html; charset=utf-8");
            res += "\nRaw: " + requestURI.getRawQuery();
            res += "\nQuery: " +requestURI.getQuery();
            Map<String, String> params = (Map<String, String>) ex.getAttribute("parameters");
            for(Map.Entry<String, String> entry: params.entrySet()) {
                res += "\n"+entry.getKey() + " => " + entry.getValue();
            }
            ex.sendResponseHeaders(200, res.length());
            OutputStream os = ex.getResponseBody();
            os.write(res.getBytes());
            os.close();
        }
    }
    class DownloadHandler implements HttpHandler {
        public void handle(HttpExchange exchange) throws IOException {
            exchange.getResponseHeaders().set("Accept-Ranges", "bytes");
            String MULTIPART_BOUNDARY = "133731337";
            String CRLF = "\r\n";
            Map<String, String> params = (Map<String, String>) exchange.getAttribute("parameters");
            if (!params.getOrDefault("filename", "").equals("")) {
//                for(Map.Entry<String, String> entry: params.entrySet()){
//                    System.out.println(entry.getKey() + " => " + entry.getValue());
//                }
                String res = params.get("filename");
                File file = new File(Settings.getInstance().STORAGE, params.get("filename"));
                if (!file.exists() || !file.getCanonicalPath().equals(file.getAbsolutePath())) { // LFI
                    exchange.sendResponseHeaders(404, -1);
                    return ;
                }
                long length = file.length();
                List<Range> ranges = this.getRanges(exchange, length);
                RandomAccessFile input = new RandomAccessFile(file, "r");
                OutputStream output = exchange.getResponseBody();
                if (ranges.size() < 2) {
                    exchange.getResponseHeaders().set("Content-Disposition", "attachment;filename=\"" + file.getName() +"\"");
                    Range range;
                    if (ranges.isEmpty()) {
                        range = new Range(0, length-1, length);
                        if (length == 0)
                            length = -1; // no chunked
                        exchange.sendResponseHeaders(200, length);
                        copy(input, output, 0, length);
                    }
                    else {
                        range = ranges.get(0);
                        exchange.getResponseHeaders().set("Content-Range", "bytes " + range.start + "-" + range.end + "/" + range.total);
                        exchange.sendResponseHeaders(206, range.length);
                    }
                    copy(input, output, range.start, range.length);
                }
                else {
                    // multi part, experience
                    exchange.getResponseHeaders().set("Content-Type", "multipart/byteranges; boundary=" + MULTIPART_BOUNDARY);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    for (Range range: ranges) {
                        // TODO: merge ranges : 1-2,2-3 -> 1-3
                        baos.write(CRLF.getBytes());
                        baos.write(("--" + MULTIPART_BOUNDARY + CRLF).getBytes());
                        baos.write(("Content-Range: bytes " + range.start + "-" + range.end + "/" + range.total+ CRLF+ CRLF).getBytes());
                        copy(input, baos, range.start, range.length);
                    }
                    baos.write(CRLF.getBytes());
                    baos.write(("--" + MULTIPART_BOUNDARY+ "--").getBytes());
                    exchange.sendResponseHeaders(206, baos.size());
                    output.write(baos.toByteArray());
                }
                output.close();
            }
            else
                exchange.sendResponseHeaders(404, -1);
        }

        private List<Range> getRanges(HttpExchange exchange, long length) {
            List<Range> ranges = new ArrayList<Range>();
            if (!exchange.getRequestHeaders().containsKey("Range"))
                return ranges;
            String range = exchange.getRequestHeaders().getFirst("Range");
//            System.out.println(range);
            if (range.startsWith("bytes=")){
                try{
                    for(String part : range.substring(6).split(",")) {
                        String[] parts = part.split("-");
                        long start = (parts.length < 1 || parts[0].equals(""))?-1: Long.parseLong(parts[0]);
                        long end = (parts.length < 2 || parts[1].equals(""))? -1: Long.parseLong(parts[1]);
                        start = (start == -1) ? 0 : start;
                        end = (end >= length ||  end == -1) ? length - 1 : end;
                        if (start > end)
                            break;
                        ranges.add(new Range(start, end, length));
                    }
                }
                catch  (NumberFormatException nex) {
                    ranges.clear();
                }
            }
            return ranges;
        }

        private void copy(RandomAccessFile input, OutputStream output, long start, long length)
                throws IOException {
            byte[] buffer = new byte[10240];
            int read;

            if (input.length() == length) {
                while ((read = input.read(buffer)) > 0) {
                    output.write(buffer, 0, read);
                }
            } else {
                input.seek(start);
                long toRead = length;
                while ((read = input.read(buffer)) > 0) {
                    if ((toRead -= read) > 0) {
                        output.write(buffer, 0, read);
                    } else {
                        output.write(buffer, 0, (int) toRead + read);
                        break;
                    }
                }
            }
        }

        protected class Range {
            long start;
            long end;
            long length;
            long total;
            public Range(long start, long end, long total) {
                this.start = start;
                this.end = end;
                this.length = end - start + 1;
                this.total = total;
            }
        }
    }
}
