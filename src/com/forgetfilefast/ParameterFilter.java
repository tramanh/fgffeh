package com.forgetfilefast;
import com.sun.net.httpserver.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class ParameterFilter extends Filter {
    @Override
    public void doFilter(HttpExchange httpExchange, Chain chain) throws IOException {
        parseGetParameter(httpExchange);
        chain.doFilter(httpExchange);
    }

    @Override
    public String description() {
        return "Parse queries: from RawQuery to HashMap";
    }

    private void parseGetParameter(HttpExchange ex) throws IOException {
        Map<String, String> parameters = new HashMap<>();

        URI requestURI = ex.getRequestURI();
        String query = requestURI.getRawQuery();
        parseQuery(query, parameters);
        ex.setAttribute("parameters", parameters);
    }
    private void parseQuery(String query, Map<String, String> parameters) throws UnsupportedEncodingException {
        if (query != null) {
            String pairs[] = query.split("[&]");
            for (String pair : pairs) {
                String param[] = pair.split("[=]");

                String key = null;
                String value = "";
                if (param.length > 0) {
                    key = URLDecoder.decode(param[0],
                            System.getProperty("file.encoding"));
                }
                if (param.length > 1) {
                    value = URLDecoder.decode(param[1],
                            System.getProperty("file.encoding"));
                }
                if (parameters.containsKey(key)) {
                    // update instead of make array
                    parameters.replace(key, value);
                }
                parameters.put(key, value);
            }
        }
    }
}
